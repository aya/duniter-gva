//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod balances;
mod current_ud;
mod last_blockstamp_out_of_fork_window;
mod members_count;
mod peers;
mod prepare_simple_payment;
mod send_txs;
mod sync;
mod utxos;

use duniter_core::crypto::keys::KeyPair;

use crate::*;

#[derive(Debug, PartialEq)]
pub(super) struct ExecReqTypeError(pub(super) String);

impl<E> From<E> for ExecReqTypeError
where
    E: ToString,
{
    fn from(e: E) -> Self {
        Self(e.to_string())
    }
}

pub(super) async fn execute_req_type(
    bda_executor: &BdaExecutor,
    req_type: BcaReqTypeV0,
    _is_whitelisted: bool,
) -> Result<BcaRespTypeV0, ExecReqTypeError> {
    match req_type {
        BcaReqTypeV0::BalancesOfPubkeys(pubkeys) => {
            balances::exec_req_balances_of_pubkeys(bda_executor, pubkeys).await
        }
        BcaReqTypeV0::BalancesOfScripts(scripts) => {
            balances::exec_req_balances_of_scripts(bda_executor, scripts).await
        }
        BcaReqTypeV0::CompressedBlockChunk { chunk_id } => {
            sync::exec_req_compressed_block_chunk(bda_executor, chunk_id).await
        }
        BcaReqTypeV0::CurrentBlockstamp => {
            if let Some(current_meta) = bda_executor.cm_accessor.get_current_meta(|cm| *cm).await {
                Ok(BcaRespTypeV0::CurrentBlockstamp(
                    current_meta.current_block_meta.blockstamp(),
                ))
            } else {
                Err("no blockchain".into())
            }
        }
        BcaReqTypeV0::CurrentUd => current_ud::exec_req_current_ud(bda_executor).await,
        BcaReqTypeV0::FirstUtxosOfPubkeys {
            amount_target_opt,
            pubkeys,
        } => utxos::exec_req_first_utxos_of_pubkeys(bda_executor, amount_target_opt, pubkeys).await,
        BcaReqTypeV0::Identities(pubkeys) => {
            let dbs_reader = bda_executor.dbs_reader();
            Ok(BcaRespTypeV0::Identities(
                bda_executor
                    .dbs_pool
                    .execute(move |dbs| {
                        pubkeys
                            .into_iter()
                            .map(|pubkey| {
                                dbs_reader.idty(&dbs.bc_db_ro, pubkey).map(|idty_opt| {
                                    idty_opt.map(|idty| Identity {
                                        is_member: idty.is_member,
                                        username: idty.username,
                                    })
                                })
                            })
                            .collect::<KvResult<ArrayVec<_, 16>>>()
                    })
                    .await??,
            ))
        }
        BcaReqTypeV0::LastBlockstampOutOfForkWindow => {
            last_blockstamp_out_of_fork_window::exec_req_last_blockstamp_out_of_fork_window(
                bda_executor,
            )
            .await
        }
        BcaReqTypeV0::MembersCount => members_count::exec_req_members_count(bda_executor).await,
        BcaReqTypeV0::PrepareSimplePayment(params) => {
            prepare_simple_payment::exec_req_prepare_simple_payment(bda_executor, params).await
        }
        BcaReqTypeV0::ProofServerPubkey { challenge } => Ok(BcaRespTypeV0::ProofServerPubkey {
            challenge,
            server_pubkey: bda_executor.self_keypair.public_key(),
            sig: bda_executor
                .self_keypair
                .generate_signator()
                .sign(&challenge),
        }),
        BcaReqTypeV0::Ping => Ok(BcaRespTypeV0::Pong),
        BcaReqTypeV0::PeersV10 { n } => peers::exec_req_peers_v1(bda_executor, n).await,
        BcaReqTypeV0::SendTxs(txs) => send_txs::send_txs(bda_executor, txs).await,
        BcaReqTypeV0::Sync { from, to } => sync::exec_req_sync(bda_executor, from, to).await,
    }
}
