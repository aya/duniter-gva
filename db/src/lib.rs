//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![deny(
    clippy::unwrap_used,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unstable_features,
    unused_import_braces
)]

mod keys;
mod values;

pub use keys::gva_utxo_id::GvaUtxoIdDbV1;
pub use keys::wallet_hash_with_bn::WalletHashWithBnV1Db;
pub use values::gva_block_db::GvaBlockDbV1;
pub use values::gva_idty_db::GvaIdtyDbV1;
pub use values::gva_tx::GvaTxDbV1;
pub use values::wallet_script_array::WalletScriptArrayV2;
pub use values::HashDb;

pub(crate) use bincode::Options as _;
pub(crate) use duniter_core::common::prelude::*;
pub(crate) use duniter_core::crypto::hashs::Hash;
pub(crate) use duniter_core::dbs::kv_typed;
pub(crate) use duniter_core::dbs::kv_typed::db_schema;
pub(crate) use duniter_core::dbs::kv_typed::prelude::*;
pub(crate) use duniter_core::dbs::smallvec::SmallVec;
pub(crate) use duniter_core::dbs::{
    bincode_db, CorruptedBytes, HashKeyV2, PubKeyKeyV2, SourceAmountValV2, ToDumpString,
    WalletConditionsV2,
};
pub(crate) use duniter_core::wallet::prelude::*;
pub(crate) use duniter_core::wot::WotId;
pub(crate) use serde::{Deserialize, Serialize};
pub(crate) use std::collections::BTreeSet;

pub const BLOCKS_CHUNK_SIZE: u32 = 4_096;

db_schema!(
    GvaV1,
    [
        ["balances", Balances, WalletConditionsV2, SourceAmountValV2],
        ["blocks_by_common_time", BlocksByCommonTime, U64BE, u32],
        ["blocks_with_ud", BlocksWithUd, U32BE, ()],
        ["blockchain_time", BlockchainTime, U32BE, u64],
        ["blocks_chunk_hash", BlocksChunkHash, U32BE, HashDb],
        ["current_blocks_chunk", CurrentBlocksChunk, U32BE, GvaBlockDbV1],
        ["gva_identities", GvaIdentities, PubKeyKeyV2, GvaIdtyDbV1],
        [
            "gva_utxos",
            GvaUtxos,
            GvaUtxoIdDbV1,
            SourceAmountValV2
        ],
        [
            "scripts_by_pubkey",
            ScriptsByPubkey,
            PubKeyKeyV2,
            WalletScriptArrayV2
        ],
        ["txs", Txs, HashKeyV2, GvaTxDbV1],
        ["txs_by_block", TxsByBlock, U32BE, Vec<Hash>],
        ["txs_by_issuer", TxsByIssuer, WalletHashWithBnV1Db, BTreeSet<Hash>],
        ["txs_by_recipient", TxsByRecipient, WalletHashWithBnV1Db, BTreeSet<Hash>],
    ]
);
