use duniter_core::crypto::{bases::b58::ToBase58, keys::Signature};

//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#[derive(Default, async_graphql::SimpleObject)]
#[graphql(name = "Peer")]
pub struct PeerCardGva {
    pub version: u32,
    pub currency: String,
    pub pubkey: String,
    pub blockstamp: String,
    pub endpoints: Vec<String>,
    pub status: String,
    pub signature: String,
    pub member: bool,
}
impl From<duniter_core::dbs::PeerCardDbV1> for PeerCardGva {
    fn from(peer_db: duniter_core::dbs::PeerCardDbV1) -> Self {
        Self {
            version: 10,
            currency: peer_db.peer.currency,
            pubkey: peer_db.peer.pubkey.to_base58(),
            blockstamp: peer_db.peer.blockstamp.to_string(),
            endpoints: peer_db.peer.endpoints.to_vec(),
            status: if peer_db.status { "UP" } else { "DOWN" }.to_owned(),
            signature: peer_db.peer.signature.to_base64(),
            member: peer_db.member,
        }
    }
}

#[derive(Default, async_graphql::SimpleObject)]
#[graphql(name = "Head")]
pub struct HeadGva {
    pub api: String,
    pub pubkey: String,
    pub blockstamp: String,
    pub software: String,
    pub software_version: String,
    pub pow_prefix: u32,
    pub free_member_room: u32,
    pub free_mirror_room: u32,
    pub signature: String,
}
impl From<duniter_core::dbs::DunpHeadDbV1> for HeadGva {
    fn from(head: duniter_core::dbs::DunpHeadDbV1) -> Self {
        Self {
            api: head.api,
            pubkey: head.pubkey.to_string(),
            blockstamp: head.blockstamp.to_string(),
            software: head.software,
            software_version: head.software_version,
            pow_prefix: head.pow_prefix,
            free_member_room: head.free_member_room,
            free_mirror_room: head.free_member_room,
            signature: head.signature.to_string(),
        }
    }
}

#[derive(async_graphql::SimpleObject)]
pub(crate) struct PeerWithHeads {
    pub peer: PeerCardGva,
    pub heads: Vec<HeadGva>,
}
